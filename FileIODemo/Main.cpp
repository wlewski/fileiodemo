#include <iostream> // used for writing to the console
#include <conio.h>
#include <string>
#include <fstream> // used for writing to file 

using namespace std;

void Print(string text, ostream &out)
{
	out << text;
}

int main()
{
	/*string name;
	cout << "Name: ";
	//cin >> name; // Usually we don't use cin for strings
	getline(cin, name); // takes name by reference 

	cout << "Your name " << name;*/

	string filePath = "C:\\temp\\tst.text";

	// Writing to a File //
	/*string input;
	getline(cin, input);

	ofstream fout(filePath); // ofs is more widely used. output file stream
	//fout.open(filePath); // can do this way 

	// error checking if fout exists
	if (fout)
	{
		fout << "You entered: \n" << input;
		fout.close(); // closes the fout object 
	}*/

	// Reading a File //
	/*string line;

	ifstream fin(filePath);
	if (fin)
	{
		while(getline(fin, line))
		{
			cout << line << "\n";
		}
		fin.close();
	}*/

	// Writing to a File and Console
	string input;
	char option;

	getline(cin, input);
	cout << "Print to (F)ile, (C)onsole, or (B)oth: ";
	cin >> option;
	if (option == 'F' || option == 'B')
	{
		ofstream fout(filePath);  

		if (fout)
		{
			Print(input, fout);
			fout.close();
		}
	}
	if (option == 'C' || option == 'B')
	{
		Print(input, cout);
	}




	_getch();
	return 0;
}